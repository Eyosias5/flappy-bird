/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extreme;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Eyosias
 */
public class EHighScoreHandling{
    public void WriteHighScore(int z) {
        String fileName = "lib/HighScoreExtreme.txt";
        try(FileWriter writer = new FileWriter(fileName); BufferedWriter bufWriter = new BufferedWriter(writer)){
            bufWriter.write(Integer.toString(z));
        }catch(IOException e){}
    }
    
    public String ReadHighScore() throws IOException{
         File file = new File("lib/HighScoreExtreme.txt");
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        String firstLine = reader.readLine();
        reader.close();
        return firstLine;
    }
}
