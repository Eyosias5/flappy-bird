/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extreme;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;


public class GamePanel extends JPanel implements Runnable {

    Game game;
    EHighScoreHandling Hscore = new EHighScoreHandling();
    EHighScoreHandling h= new EHighScoreHandling();
    
    public GamePanel() {
        game = new Game();
        new Thread(this).start();
    }

    public void update() {
        game.update();
        repaint();
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2D = (Graphics2D) g;
        for (Render r : game.getRenders())
          /** if (r.transform != null)
                g2D.drawImage(r.image, r.transform, null);
            else**/
                g.drawImage(r.image, r.x, r.y, null);


        g2D.setColor(Color.BLACK);

        if (!game.started) {
            g2D.setFont(new Font("TimesRoman", Font.PLAIN, 20));
            g2D.drawString("SPACE", 205, 270);
            g2D.drawImage(ImageLoader.getImage("lib/play1.png"), 200, 180,70,70,null);
            
            
        } else {
            g2D.setFont(new Font("TimesRoman", Font.PLAIN, 22));
            g2D.drawString(Integer.toString(game.score), 10, 30);
            g2D.drawString("P--Pause", 40,30);
        }
        if (game.paused && !game.gameover){
        g2D.drawImage(ImageLoader.getImage("lib/pause2.png"), 200, 180,70,70,null);
        
        }    
        if (game.gameover) {
            g2D.setFont(new Font("TimesRoman", Font.PLAIN, 20));
            g2D.drawString("Press R to restart", 150, 270);
            g2D.drawImage(ImageLoader.getImage("lib/ret.png"), 150, 180,70,70,null);
            try {
                if(Integer.parseInt(h.ReadHighScore())<(game.score)){
                    Hscore.WriteHighScore(game.score);
                }
            } catch (IOException ex){}
            g2D.drawString("SCORE: "+ Integer.toString(game.score),150,80);
            try {
                g2D.drawString("High Score: "+Integer.parseInt(h.ReadHighScore()),138, 120);
            } catch (IOException ex) {}
        }
    }

    public void run() {
        try {
            while (true) {
                update();
                Thread.sleep(15);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}

