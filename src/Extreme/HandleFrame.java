/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extreme;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class HandleFrame {
    public static int WIDTH = 500;
    public static int HEIGHT = 500;
    public JFrame frame;
    private JButton Classic;
    private JButton Extreme;
    private JLabel label;
    JFrame menu ;
    GamePanel panel;
    JPanel p;
    ImageLoader i;
    public classic.APP app;
    public  void go(){
        frame = new JFrame();
        menu= new JFrame();
        p = new JPanel();
        frame.setResizable(true);
        menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        menu.setLocation(WIDTH-46, WIDTH-363);
        menu.setSize(WIDTH, HEIGHT);
        
        Classic = new JButton("CLassic");
        label = new JLabel("WELCOME TO FLAPPY BIRD");
        label.setForeground(Color.DARK_GRAY.darker());
        label.setBackground(Color.lightGray.brighter());
        label.setFont(new Font("Algerian", Font.PLAIN, 20));
        p.add(label);
        Extreme = new JButton("Extreme");
        Classic.setBackground(Color.GRAY.darker());
        Classic.setForeground(Color.WHITE);
        Extreme.setBackground(Color.GRAY.darker() );
        Extreme.setForeground(Color.WHITE);
        Extreme.addActionListener(new ExtremeListener());
        Classic.addActionListener(new ClassicListener());
        
        p.add(Classic);
        
        p.setBackground(Color.GREEN.darker());
        p.add(Extreme);
        
        
       
        menu.setVisible(true);
        
        menu.add(p);
        
        
        frame.setVisible(false);
        
    }
   

   class ExtremeListener implements ActionListener {
   
    public void actionPerformed(ActionEvent ev) {
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(WIDTH, HEIGHT);
        frame.setLocationRelativeTo(null);
        Keyboard keyboard = Keyboard.getInstance();
        frame.addKeyListener(keyboard);
        menu.setVisible(false);
        frame.setVisible(true);
           panel = new GamePanel();
           frame.add(panel);
}
}
       class ClassicListener implements ActionListener {

         public void actionPerformed(ActionEvent ev) {
         app = new classic.APP();
         app.go();
         menu.setVisible(false);
         }
    }
    
}
