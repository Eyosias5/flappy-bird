
package Extreme;

import java.awt.Image;
import java.awt.event.KeyEvent;


public class Bird extends Component{


    public boolean dead;

    public double velocity;
    public double gravity;

    private int jumpDelay;
   

    private Image image;
    private Keyboard keyboard;

    public Bird() {
        x = 100;
        y = 150;
        velocity = 0;
        width = 45;
        height = 32;
        gravity = 0.5;
        jumpDelay = 0;
       
        dead = false;

        keyboard = Keyboard.getInstance();
    }

    public void update() {
        velocity += gravity;

        if (jumpDelay > 0)
            jumpDelay--;

        if (!dead && keyboard.isDown(KeyEvent.VK_SPACE) && jumpDelay <= 0) {
            velocity = -10;
            jumpDelay = 10;
        }

        y += (int)velocity;
    }

    public Render getRender() {
        Render render = new Render();
        render.x = x;
        render.y = y;

        if (image == null) {
            image = ImageLoader.getImage("lib/bb.png");     
        }
        render.image = image;

      
        return render;
    }
}
