/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extreme;

/**
 *
 * @author IMMAN_AY
 */
public abstract class Component {
    public int x;
    public int y;
    public int width;
    public int height;
    public abstract void update();
    public abstract Render getRender(); 
}
