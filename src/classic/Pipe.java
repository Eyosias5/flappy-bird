
package classic;

import java.awt.Image;


public class Pipe extends Component{
   
    public int speed = 3;

    public String orientation;

    private Image image;

    public Pipe(String orientation) {
        this.orientation = orientation;
        reset();
    }

    public void reset() {
        width = 66;
        height = 400;
        x = APP.WIDTH + 2; // x position of the first pipe to be seen. It is set out of the screen so that it gradually appears from the right.

        if (orientation.equals("south")) {
            y = -(int)(Math.random() * 120) - height / 2;
        }
    }

    public void update() {
        x -= speed;
    }

    public boolean collides(int _x, int _y, int _width, int _height) { //All parameters here are the birda x,y,width and height position

        int margin = 2;

        if (_x + _width - margin > x && _x + margin < x + width) { // checks if the bird is in between the pipes

            if (orientation.equals("south") && _y < y + height) { //if the postion of the bird is higher than the top pipe 
                return true;
            } else if (orientation.equals("north") && _y + _height > y) { //if the position of the bird is lower than the psotion of the botton pipe
                return true;
            }
        }

        return false;
    }

    public Render getRender() {
        Render r = new Render();
        r.x = x;
        r.y = y;

        if (image == null) {
            image = ImageLoader.loadImage("lib/pipe-" + orientation + ".png");
        }
        r.image = image;

        return r;
    }
}
