/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classic;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Eyosias
 */
public class HighScoreHandling{
    public void WriteHighScore(int z) {//this method is called at the GamePanel
        String fileName="lib/highScore.txt"; 
        try(FileWriter writer = new FileWriter(fileName); BufferedWriter bufWriter = new BufferedWriter(writer)){ // creates a .txt file called highScore.txt in your lib folder. 
            bufWriter.write(Integer.toString(z)); // write the the number z (which is you high schore)
        }catch(IOException e){}
    }
    
    public String ReadHighScore() throws IOException{ // this method is called at the GamePanel
         File file = new File("lib/highScore.txt");
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        String firstLine = reader.readLine();// Read the whats in the file
        reader.close();// close the txt file after done reading.
        return firstLine;
    }
}
