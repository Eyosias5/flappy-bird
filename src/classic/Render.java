
package classic;

import java.awt.Image;

public class Render { // sets the postion and images  called in bird and pip object
    public int x;
    public int y;
    public Image image;

    public Render() {}

    public Render(int x, int y, String imagePath) {
        this.x = x;
        this.y = y;
        this.image = ImageLoader.loadImage(imagePath);
    }
}
