
package classic;

import java.awt.Image;
import java.awt.event.KeyEvent;

public class Bird extends Component {
   
    public boolean dead;
    public double yvel;
    public double gravity;
    private int jumpDelay;
    private Image image;
    private Keyboard keyboard;

    public Bird() {//This method runs first when Bird object is called from the Game object.
        //Note that the co-ordinates of the screen starts fromt the top left corner of the screen. Meaning (0,0) is at the top left corner.
        x = 100; //x position for the bird
        y = 150;//y posion for the bird
        yvel = 0; //this the changing Y value when the bird falls.
        width = 45;
        height = 32;
        gravity = 0.5; //y postion of the bird decreases by 0.5 every time. To make it seem its always falling.
        jumpDelay = 0;// This gives Delay's in the jumps of the bird. Meaning you can press the space button rapidly and expect the bird's position to change.
        dead = false;// is the bird dead or not. is set to true if it touches the pipe.

        keyboard = Keyboard.getInstance();//a listner for keyboard clicks
    }

    public void update() {//This method is called from the Game object so this runs after the consturcor. Dont forget that this method is called constantly.
        yvel += gravity; //adds the gravity to the changing y value

        if (jumpDelay > 0) //jumpDelay 
          jumpDelay--; //this decreases the jumpDelay by One. Note its 10 when you press the space bar.

        if (!dead && keyboard.isDown(KeyEvent.VK_SPACE) && jumpDelay <= 0) { // if the bird is not dead and the space button is clicked
            yvel = -10; // the changing y  position is subtracted 10 (Showning an act of jumping
            jumpDelay = 10; // set the jumpDelay to 10
        }

        y += (int)yvel; //set the position of the bird's y position to the new y postion.
    }

    public Render getRender() {
        Render r = new Render();
        r.x = x;
        r.y = y;

        if (image == null) {
            image = ImageLoader.loadImage("lib/bird.png");     
        }
        r.image = image;       
        return r;
    }
}