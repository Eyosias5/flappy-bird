package classic;

import javax.swing.JFrame;

public class APP {
    public static int WIDTH = 500;
    public static int HEIGHT = 500;
    public static APP a;
    public JFrame frame;
    public static void main(String[] args) {
        
    // a= new APP();
      a.go(); //Since APP is set as static No need to create a new object
    }
    public void go(){
        frame = new JFrame(); // Create a JFrame Object
        frame.setVisible(true);//set the frame as visible
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// Make an X button at the top
        frame.setSize(WIDTH, HEIGHT);// set the height of the frame
        frame.setLocationRelativeTo(null);//

        Keyboard keyboard = Keyboard.getInstance();
        frame.addKeyListener(keyboard); //add key listeners for keyboards
        frame.setResizable(false);
        Gamepanel panel = new Gamepanel();//
        frame.add(panel);//Adding the panel to the frame .THis is where it all starts. Got to Gamepanel now.
    
    }
    
}
