
package classic;

import java.awt.event.KeyEvent;
import java.util.ArrayList;


public class Game {
    public static final int PIPE_DELAY = 100;
    public Boolean paused;
    private int pauseDelay;
    private int restartDelay;
    private int pipeDelay;
    private Bird bird;
    private ArrayList<Pipe> pipes;
    private Keyboard keyboard;
    public int score;
    public Boolean gameover;
    public Boolean started;

    public Game() {//Game() method runs first since its the constructor.
        keyboard = Keyboard.getInstance(); //getting the keyboard instances keyboard instance
        restart();//basically sets all the variables declared above to there defualt set.
    }

    public void restart() {
        paused = false;
        started = false;
        gameover = false;

        score = 0;
        pauseDelay = 0;
        restartDelay = 0;
        pipeDelay = 0;

        bird = new Bird();//bird object created
        pipes = new ArrayList<Pipe>();//Array of pipes created
    }

     public void update() {//notice this is the method called from GamePanel
        watchForStart();//This method checks if the game has started and sets "started" as true or false.
                        //Go to watchForStart method to see what it does.

        if (!started)// "started is either true or false depending on what "watchForStart" returns
            return;// gets out of the method update();

        watchForPause();// "paused is either true or false depending on what "watchForPause" returns
        watchForReset();// "gameover is either true or false depending on what "watchForReset" returns

        if (paused)
            return;// gets out of the method update();

        bird.update();//This is where the birds Jump functionality keeps getting rendered. Go to the Bird class to see 

        if (gameover)
            return;// gets out of the method update();

        movePipes();//Moves the pipes towards the bird. Note that the bird doesnt move. Rather the pipers are drawn closer and closer.
        checkForCollisions();//check weather or not the bird is on contact with the pipe.
        
        // NEXT, GO TO movePipes() after this.
    }

    public ArrayList<Render> getRenders() { // Renders all the objects inlcuding the foreground and the backgoround
        ArrayList<Render> renders = new ArrayList<Render>(); // creating array list of renders
        renders.add(new Render(0, 0, "lib/o.png"));// this is the background
        for (Pipe pipe : pipes)
            renders.add(pipe.getRender());  // add each pipe render to an array list
        renders.add(new Render(0, 0, "lib/foreground.png")); //this is the foreground
        renders.add(bird.getRender()); // adding the bird render to an array list
        return renders;
    }

    private void watchForStart() {
        if (!started && keyboard.isDown(KeyEvent.VK_SPACE)) {//basically means if the game has no started and user presses the "Space" button.
            started = true;//sets started as true.
        }
    }

    private void watchForPause() {
        if (pauseDelay > 0)
            pauseDelay--;

        if (keyboard.isDown(KeyEvent.VK_P) && pauseDelay <= 0) {//if Keyboard P is pressed down  the is delayed is < 0 , paused will be set to its opposite value.
            paused = !paused;
            pauseDelay = 10;
        }
    }

    private void watchForReset() {
        if (restartDelay > 0)
            restartDelay--;

        if (keyboard.isDown(KeyEvent.VK_R) && restartDelay <= 0) {
            restart();
            restartDelay = 10;
            return;
        }
    }

    private void movePipes() {
        pipeDelay--;

        if (pipeDelay < 0) {
            pipeDelay = PIPE_DELAY;
            Pipe northPipe = null; // set an empty variable for the pipe coming from the north
            Pipe southPipe = null; // set an empty variable for the pipe coming from the south

            if (northPipe == null) { 
                Pipe pipe = new Pipe("north"); // add a Pipe with an orientation of 'north' to the pipe variable (North Pipes are pipes at the bottom)
                pipes.add(pipe); // put it in an array of pipes.
                northPipe = pipe; // set the northpipe as the pipe you just created
            } else {
                northPipe.reset();// if there is a pip in the northPipe variable se
            }

            if (southPipe == null) { //samething as north. (South Pipes are pipes at the top)
                Pipe pipe = new Pipe("south");
                pipes.add(pipe);
                southPipe = pipe;
            } else {
                southPipe.reset();
            }

            northPipe.y = southPipe.y + southPipe.height + 175;// THis is the spacing between the north and south pipes. change "175" to 80 to see the difference
        }

        for (Pipe pipe : pipes) { // finally for the array of pipes you have just added above
            pipe.update(); // change the x postion set in the pipe class. To show a sense of movement form right to the left
        }
    }

    private void checkForCollisions() {

        for (Pipe pipe : pipes) {
            if (pipe.collides(bird.x, bird.y, bird.width, bird.height)) { //return's true or false by checking if the bird touched the pipe.
                gameover = true;  
                bird.dead = true;
            } else if (pipe.x == bird.x && pipe.orientation.equalsIgnoreCase("south")) { // as soon as the bird's tale passes the pipe
                score++;  // increase the score
            }
            System.out.println("");
        }

        // Ground + Bird collision
        if (bird.y + bird.height > APP.HEIGHT - 80) {
            gameover = true;
            bird.y = APP.HEIGHT - 80 - bird.height;
        }
    }
}