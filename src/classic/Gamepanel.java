
package classic;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.IOException;
import javax.swing.JPanel;

public class Gamepanel extends JPanel implements Runnable{ // notice it implements Runnable. Which makes this class a thread.
    Game game;                                             
    HighScoreHandling Hscore = new HighScoreHandling();
    HighScoreHandling h= new HighScoreHandling();
 

    public Gamepanel() {//This gets runs first when object is instantiated since its a constructor.
        game = new Game();//instantiates the game. dont go to the Game object yet.
        new Thread(this).start();// Thread(this) "this" represents this class im in.
                                 // When a thread starts, it goes to the run() method. so go to ther run method below().
    }

    public void update() {
        game.update();//this where all the pipes , birds objects are. Now go to the Game object.
        repaint(); //it clears the screen before it get updated again.
    }

    protected void paintComponent(Graphics g) { // here is everything written on the game.
        super.paintComponent(g);

        Graphics2D g2D = (Graphics2D) g; // this is basically a  color full pen.
        for (Render r : game.getRenders()) // this where an array list of renders are called and drawn 
        g.drawImage(r.image, r.x, r.y, null);


        //g2D.setColor(Color.BLACK);

        if (!game.started) { // before game has started this is the start screen
            g2D.setFont(new Font("TimesRoman", Font.PLAIN, 20));
            g2D.drawString("Press SPACE to start", 150, 240);
            g2D.drawImage(ImageLoader.loadImage("lib/play1.png"), 200, 150,70,70,null);
        } else {// when the game has started display the score and "P-Pause" at the given coordinates
            g2D.setFont(new Font("TimesRoman", Font.PLAIN, 24));
            g2D.drawString(Integer.toString(game.score), 10, 30);
            g2D.drawString("P--Pause", 40,30);
        }
        if (game.paused && !game.gameover){ // game is paused and not ended display that logo
        g2D.drawImage(ImageLoader.loadImage("lib/pause2.png"), 200, 180,70,70,null);
        
        }   
        if (game.gameover) { // when game is over write the texts below on the given coordinates
            g2D.setFont(new Font("TimesRoman", Font.PLAIN, 20));
            g2D.drawString("Press R to restart", 150, 240);
            g2D.drawImage(ImageLoader.loadImage("lib/ret.png"), 180, 140,70,70,null);
            try {
                if(Integer.parseInt(h.ReadHighScore())<(game.score)){
                    Hscore.WriteHighScore(game.score); // highscore is handled by file handling. so go to the Highscore Handling class to see how its handled.
                }
            } catch (IOException ex){}
            g2D.drawString("SCORE: "+ Integer.toString(game.score),150,280);
            try {
                g2D.drawString("High Score: "+Integer.parseInt(h.ReadHighScore()),150, 330);
            } catch (IOException ex) {}
        }
    }

    public void run() {
        try {
            while (true) {//while true basically means forever.
                update();//bascially the drawing of the images on the panel.check update method.
                Thread.sleep(25); //thread sleeps for 25 millliseconds before it runs again
                                   //Change to fasten the game up
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
